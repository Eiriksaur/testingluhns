import luhn.*;

import java.util.ArrayList;
import java.util.Scanner;


public class Program {

    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);
        LuhnAlgorithm luhn = new LuhnAlgorithm();

        System.out.println("Enter e creditcard number (16 digits)");
        String theInput = sc.nextLine();
        System.out.println("Input: " + luhn.generateConsoleString(theInput));//Getting the right format
        ArrayList<String> cardNrString = new ArrayList<String>();
        Boolean lengthCheck = luhn.checkLength(theInput, cardNrString);
        if(!lengthCheck){ // will exit if the entered number does not meet the requirements
            System.out.println("Cardnumber not the correct length");
            System.exit(0);
        }

        try {
            System.out.println("Provided checkDigit: " + luhn.getProvidedCheckDigit(cardNrString));
            ArrayList<Integer> cardNrInt = luhn.convertToInt(cardNrString);
            System.out.println("Expected checkDigit: " + luhn.findCheckDigit(cardNrInt));

            String res = luhn.luhnAlg(cardNrInt) ? "Valid" : "Unvalid";

            System.out.println("Checksum: " + res);

        } catch (NumberFormatException e){
            System.out.println("Wrong input");
            System.out.println(e.toString());
        }
    }
}
