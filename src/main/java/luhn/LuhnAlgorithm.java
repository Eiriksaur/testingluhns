package luhn;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class LuhnAlgorithm {

    //Run a weighted pattern 2.1.2.1.---- every answer that gives a 2 digit answer we add the 2 digits.
    //If the sum of all the numbers ends with a zero the number is valid.

    //Generate the string that whould be written to console
    public String generateConsoleString(String inputString){
        String res = "";
        StringBuffer strbuff = new StringBuffer(inputString);
        strbuff.insert(inputString.length()-1, " ");
        res = strbuff.toString();
        return res;
    }

    //Checking that the length of the input matches creditcard length
    public boolean checkLength(String inputString, ArrayList<String> inputCardnr){
        String[] temp = inputString.split("");
        for(int i = 0; i < temp.length; i++){
            inputCardnr.add(temp[i]);
        }
        if(inputCardnr.size() == 16) return true;
        return false;
    }

    //Getting and returning the last digit in the number
    public int getProvidedCheckDigit(ArrayList<String> cardNrString){
        try {
            return Integer.parseInt(cardNrString.get(cardNrString.size()-1));
        } catch (NumberFormatException e){
            throw e;
        }
    }

    //Converts the string input to a list of integers
    public ArrayList<Integer> convertToInt(ArrayList<String> inputCardnr){
        ArrayList<Integer> res = new ArrayList<>();
        try {
            for (int i = 0; i < inputCardnr.size(); i++) {
                res.add(Integer.parseInt(inputCardnr.get(i)));
            }
        }catch (NumberFormatException e){
            throw e;
        }
        return res;
    }

    //Figuring out and returning what the acutall check digit should be
    public int findCheckDigit(ArrayList<Integer> cardnr){
        int sum = 0;
        for(int i = 0; i < cardnr.size()-1; i++){
            if(i % 2 == 0){
                if(cardnr.get(i) * 2 >= 10){
                    int tempRes= cardnr.get(i) * 2;
                    sum += (1 + tempRes%10);
                }else {
                    sum += cardnr.get(i) * 2;
                }
            }else{
                sum += cardnr.get(i) * 1;
            }
        }
        int rest = 10 - (sum % 10);
        if(rest == 10) rest = 0;
        return rest;
    }

    //Checking wheter or not the number is a valid creditcard number
    public boolean luhnAlg(ArrayList<Integer> cardnr){
       boolean res = false;
       int sum = 0;
       for(int i = 0; i < cardnr.size(); i++){
           if(i % 2 == 0){
               if(cardnr.get(i) * 2 >= 10){
                   int tempRes= cardnr.get(i) * 2;
                   sum += (1 + tempRes%10);
               }else {
                   sum += cardnr.get(i) * 2;
               }
           }else{
               sum += cardnr.get(i) * 1;
           }
       }
       if(sum % 10 == 0) res = true;
       return res;
    }
}
