package luhn;

import com.sun.jdi.ArrayReference;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class LuhnAlgorithmTest {

    LuhnAlgorithm luhnTest = new LuhnAlgorithm();
    @Test
    void checkLength() {
        String testInput = "1234567891234567";
        ArrayList<String> testContainer = new ArrayList<>();
        assertTrue(luhnTest.checkLength(testInput, testContainer));
        testInput = "123456789123456712323";
        assertFalse(luhnTest.checkLength(testInput, testContainer));
    }

    @Test
    void convertToInt() {
        ArrayList<String> stringToConvert = new ArrayList<>();
        stringToConvert.add("1");
        stringToConvert.add("2");
        stringToConvert.add("Fail");
        assertThrows(NumberFormatException.class, () -> {
            luhnTest.convertToInt(stringToConvert);
        });
        stringToConvert.set(2,"3");
        ArrayList<Integer> convertedInts = new ArrayList<>();
        convertedInts.add(1);
        convertedInts.add(2);
        convertedInts.add(3);
        assertTrue(convertedInts.equals(luhnTest.convertToInt(stringToConvert)));

    }

    @Test
    void luhnAlg() {
        ArrayList<Integer> cardnr = new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,3));
        assertTrue(luhnTest.luhnAlg(cardnr));
        cardnr.remove(cardnr.size()-1);
        cardnr.add(1);
        assertFalse(luhnTest.luhnAlg(cardnr));
    }

    @Test
    void generateConsoleString(){
        String testString = "1234567";
        assertTrue(luhnTest.generateConsoleString(testString).equals("123456 7"));
        assertFalse(luhnTest.generateConsoleString(testString).equals("1234567"));
    }

    @Test
    void findCheckDigit(){
        ArrayList<Integer> cardnr = new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,3));
        assertTrue(luhnTest.findCheckDigit(cardnr) == 3);
        assertFalse(luhnTest.findCheckDigit(cardnr) == 9);
    }

    @Test
    void getProvidedCheckDigit(){
        ArrayList<String> stringToCheck = new ArrayList<>();
        stringToCheck.add("1");
        stringToCheck.add("2");
        stringToCheck.add("3");
        assertTrue(luhnTest.getProvidedCheckDigit(stringToCheck) == 3);
    }
}